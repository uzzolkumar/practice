-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2015 at 12:17 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `computer_dept`
--

-- --------------------------------------------------------

--
-- Table structure for table `first_semester`
--

CREATE TABLE `first_semester` (
  `ID` int(250) NOT NULL,
  `Subject_code` int(250) NOT NULL,
  `Name_of_the_subject` varchar(250) NOT NULL,
  `theory_Cont_assess` varchar(250) NOT NULL,
  `theory_Final_exam` varchar(250) NOT NULL,
  `Practical_Cont_assess` varchar(250) NOT NULL,
  `Practical_Final  exam` varchar(250) NOT NULL,
  `Total_marks` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `first_semester`
--

INSERT INTO `first_semester` (`ID`, `Subject_code`, `Name_of_the_subject`, `theory_Cont_assess`, `theory_Final_exam`, `Practical_Cont_assess`, `Practical_Final  exam`, `Total_marks`) VALUES
(1, 1011, 'Engineering Drawing ', '-', '-', '50', '50', '100'),
(2, 5712, 'English -1', '20', '80', '-	', '-	', '100'),
(3, 5911, 'Mathematics-1', '30', '120', '50', '-', '200'),
(4, 5913, 'Chemistry', '30', '120', '25', '25', '200'),
(5, 6611, 'Computer Fundamental', '20', '80', '25', '25', '150'),
(6, 6711, 'Basic Electricity ', '30', '120', '25', '25', '200'),
(7, 6811, 'Basic Electronics ', '20', '80', '25', '25', '150');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `first_semester`
--
ALTER TABLE `first_semester`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `first_semester`
--
ALTER TABLE `first_semester`
  MODIFY `ID` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
